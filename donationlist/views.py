from django.shortcuts import render

# Create your views here.
def donationlist(request):
    return render(request, 'donationlist.html')
