from django.apps import AppConfig


class DonationlistConfig(AppConfig):
    name = 'donationlist'
