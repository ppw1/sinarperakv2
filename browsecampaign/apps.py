from django.apps import AppConfig


class BrowsecampaignConfig(AppConfig):
    name = 'browsecampaign'
