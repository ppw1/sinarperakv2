from django.shortcuts import render
from django.http import HttpResponseRedirect
from createcampaign.models import
# Create your views here.
response = {}
def campaignPage(request):
	return render(request, 'campaignPage.html')

def browseCampaign(request):
	campaigns = Campaign.objects.all()
	response['campaigns'] = campaigns
	html = 'browseCampaign.html'
	return render(request, html, response)
