from django.urls import path
from django.conf.urls import url
from .views import *

app_name = 'login'

urlpatterns = [
    path('logout/', logout, name="logout"),
]
