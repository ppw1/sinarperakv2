from django.shortcuts import render
from django.shortcuts import render, redirect 
from .models import Testimoni 
from datetime import datetime 
import pytz 
import json
# Create your views here.
data_dict = {}
def index(request):
	data_dict = Testimoni.objects.all().values()
	return render(request,'testimoni.html',{'data_dict': }

def add_testimoni(request):
	if request.method == 'POST':
		act = request.POST['testimony']
		Testimoni.objects.create(testimony = act)
		return redirect('/testimoni/')
	
def convert_to_json(queryset):
	val = []
	for data in queryset:
		val.append(data)
	return val
		