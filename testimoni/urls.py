from django.urls import path
from . import views

urlpatterns = [
        path('testimoni', views.index, name='index'),
        path('tambah', views.add_testimoni, name= 'tambah'),
]
