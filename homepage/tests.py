from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import Home

class IndexUnitTest(TestCase):
    def test_home_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_index_html_text(self):
        request = HttpRequest()
        response = Home(request)
        html_response = response.content.decode('utf8')
        self.assertIn('News', html_response)