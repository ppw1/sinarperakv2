from django import forms

from .models import Campaign

class PostCampaign(forms.ModelForm):

    class Meta:
        model = Campaign
        fields = ('Project_name', 'Goal', 'Donation_end_date', 'Description','image')