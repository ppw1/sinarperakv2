from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import Campaign
from .forms import PostCampaign
# Create your views here.
response = {}

def createCampaign(request):
	campaigns = Campaign.objects.all()
	response['campaigns'] = campaigns

	html = 'Create-Campaign.html'
	return render(request, html, response)
	
def savecampaign(request):
	form = PostCampaign(request.POST or None)
	if(request.method == 'POST'):
		response['Project_name'] = request.POST['Project_name']
		response['Goal'] = request.POST['Goal']
		response['Donation_end_date'] = request.POST['Donation_end_date']
		response['Description'] = request.POST['Description']
		response['image']=request.POST['image']
		myCampaign = Campaign(Project_name=response['Project_name'], Goal=response['Goal'], Donation_end_date=response['Donation_end_date'], Description=response['Description'],image=response['image'])
		myCampaign.save()
		return HttpResponseRedirect('/')
	else:
		return HttpResponseRedirect('/')
