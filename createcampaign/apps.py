from django.apps import AppConfig


class CreateCampaignConfig(AppConfig):
    name = 'create_campaign'
